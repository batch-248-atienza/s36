//contains all the endpoints for our application
//we separate the routes such that "index.js" only contains information on the server
//we need to use express' Router() function to achieve this
const express = require("express");

//creates a router instance that functions as a middleware and routing system
//makes it easier to create routes for our application
const router = express.Router();

//the taskController allows us to use the functions defined in the task controller file

const taskController = require("../controllers/taskControllers");

//Route
//The routes are responsible for defining the URIs that our client accesses and the corresponding controller functions that will be used when a certain route is accessed
//all the business logic is done in the controller

//route to get all the tasks
//this route expects to receive a GET request at the URL "/tasks"

router.get("/",(req,res)=>{

	//taskController.getAllTasks();
	//resultFromController is only used here to make the code easier to understand
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

//Mini Activity Start

// Route to create a new task
// This route expects to receive a POST request at the URL "/tasks"
// The whole URL is at "http://localhost:3001/tasks"
router.post("/", (req, res) => {

	// The "createTask" function needs the data from the request body, so we need to supply it to the function
	// If information will be coming from the client side commonly from forms, the data can be accessed from the request "body" property
	//taskController.createTask(req.body);
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
	
});

//Mini-Activity End

//Route to delete a task
//This route expects to receive a DELETE request at the URL "/tasks/:id"
//The whole URL is at "http://localhost:4000/tasks/:id"

router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//Route to update a task
//This routes expects to receive a PUT request at the URL "/tasks/:id"
//The whole URL is at "http://localhost:4000/tasks/:id"

router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
});

/*
====================
=  ACTIVITY START  =
====================
*/

//GET request at "/tasks/:id" to get a specific task
router.get("/:id", (req, res) => {
	taskController.getTask(req.params.id).then(resultFromController => res.send(resultFromController));
});

//PUT request at "/tasks/:id/complete" to update a task
router.put("/:id/complete", (req, res) => {
	taskController.updateTaskStatus(req.params.id).then(resultFromController => res.send(resultFromController));
});

/*
====================
=   ACTIVITY END   =
====================
*/

//module.exports to export the router object to use in the index.js
module.exports = router;

