//Setup the dependencies
const express = require("express");
const mongoose = require("mongoose");

//this allows us to use all the routes defined in taskRoutes.js
const taskRoutes = require("./routes/taskRoutes");

const login = require("./login");

//Server Setup
const app = express();
const port = 4000;

app.use(express.json());
app.use(express.urlencoded({extended:true}));

//Database Connection
//Connecting to MongoDB Atlas
const dbUser = "admin";
const dbPassword = "admin123";
const dbCollection = "s35";

mongoose.set('strictQuery',true);

mongoose.connect(`mongodb+srv://${login.user}:${login.password}@clusterbatch248.ogkkhkk.mongodb.net/${login.collection}?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

//connection to database
let db = mongoose.connection;

db.on("error",console.error.bind(console,"connection error"));
db.once("open",()=>console.log("Hi! We are connected to MongoDB Atlas!"));

//Add the task route
//Allows all the task routes created in the taskRoutes.js file to use "/tasks" route
app.use("/tasks",taskRoutes)

//Server listening
app.listen(port,()=>console.log(`Now listening to port ${port}`));